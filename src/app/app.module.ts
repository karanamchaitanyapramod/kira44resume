import { BrowserModule } from '@angular/platform-browser';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { NgModule } from '@angular/core';

import {MatTooltipModule} from '@angular/material/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { AboutComponent } from './profile/about/about.component';
import { ExperienceComponent } from './profile/experience/experience.component';
import { HomeComponent } from './profile/home/home.component';
import { EducationComponent } from './profile/education/education.component';
import { SkillsComponent } from './profile/skills/skills.component';
import { ContactComponent } from './profile/contact/contact.component';
import { NavbarComponent } from './profile/navbar/navbar.component';
import {MatButtonModule} from '@angular/material/button'
import {MatDialogModule} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ParticlesModule } from 'angular-particle';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { ExperienceboxComponent } from './profile/experience/experiencebox/experiencebox.component';
import { FooterComponent } from './profile/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    AboutComponent,
    ExperienceComponent,
    HomeComponent,
    EducationComponent,
    SkillsComponent,
    ContactComponent,
    NavbarComponent,
    ExperienceboxComponent,
    FooterComponent,
  ],
  entryComponents: [
 ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ParticlesModule,
    MatTooltipModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      outerStrokeWidth: 24,
      innerStrokeWidth: 0,

      outerStrokeColor: "#f44336",
      innerStrokeColor: "#FFFF",
      animationDuration:500,
      animation:true,
      clockwise:false,
      responsive:false,
      showTitle:true,
      titleFontSize:"32",
      showInnerStroke:true,
      showSubtitle:false
    }),
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatIconModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
