import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormControl,Validators} from '@angular/forms'


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private fb:FormBuilder) { }
  public afterSubmission:any
  public validName:Boolean=false
  public validContact:Boolean=false
  public validEmail:Boolean=false
  public validMessage:Boolean=false
  public buttonclass:any
  public buttonColor:any
  public detailsSent=false
  public noerror:Boolean=true
  ngOnInit() {
  }
  contactForm=this.fb.group({
    name:['',[Validators.required]],
    contactNo:['',[Validators.required,Validators.maxLength(10),Validators.minLength(10),Validators.pattern(/^[6-9]/)]],
    emailId:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    message:['',[Validators.required]]
  })
  changeNameLabel(){
    if(this.contactForm.value.name!="" && this.contactForm.value.name!=undefined){
      this.validName=true
    }
    else{
      this.validName=false
    }
  }
  changeContactLabel(){
    if(this.contactForm.value.contactNo!="" && this.contactForm.value.contactNo!=undefined){
      this.validContact=true
    }
    else{
      this.validContact=false
    }

  }
  changeEmailLabel(){
    if(this.contactForm.value.emailId!="" && this.contactForm.value.emailId!=undefined){
      this.validEmail=true
    }
    else{
      this.validEmail=false
    }

  }
  changeMessageLabel(){
    if(this.contactForm.value.message!="" && this.contactForm.value.message!=undefined){
      this.validMessage=true
    }
    else{
      this.validMessage=false
    }
  }
  formSubmitted(){
    if(this.contactForm.valid){
      this.buttonclass={
        "success":true,
        "animate":true
      }
      setTimeout(()=>this.buttonColor={
        "backgroundColor":"#29B966"
      },2500)
      setTimeout(()=>{
       this.detailsSent=true
      },3500)
    }
    else{
     setTimeout(()=>{
       this.noerror=false
     },1500)
      this.buttonclass={
        "error":true,
        "animate":true
      }
      setTimeout(()=>{
        this.buttonclass={
          "error":false,
          "animate":false,
          "success":false,
        }
      },6000)
    }
  }
  formReset(){
    this.noerror=true
    this.contactForm.reset()
    this.changeNameLabel()
    this.changeMessageLabel()
    this.changeEmailLabel()
    this.changeContactLabel()
    this.buttonColor=""

    this.buttonclass={
      "error":false,
      "animate":false,
      "success":false,
    }
  }
}
