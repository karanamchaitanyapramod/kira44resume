import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  constructor() { }
  public navbarOpen=false
  public count=0
  ngOnInit() {
  }
  public isNavbarCollapsed:any
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  burgertEvent(){
    this.count+=1
    if(this.count%2!=0){
      this.navbarOpen=true
    }
    else{
      this.navbarOpen=false
    }
  }

}
