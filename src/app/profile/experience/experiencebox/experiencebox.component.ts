import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-experiencebox',
  templateUrl: './experiencebox.component.html',
  styleUrls: ['./experiencebox.component.css']
})
export class ExperienceboxComponent implements OnInit {
  @Input() view:any
  public tabView=false
  public desktopView=false

  constructor() { }

  ngOnInit() {
    if(this.view=="tabView"){
      this.tabView=true
    }
    else{
      this.desktopView=true
    }
  }


}
