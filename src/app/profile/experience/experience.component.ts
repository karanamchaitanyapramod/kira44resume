import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {
  public innerWidth:any
  public desktopView:any
  public tabView:any
  public dv="desktopView"
  public tv="tabView"
  constructor() { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.screenWidth(this.innerWidth)
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.screenWidth(this.innerWidth)
  }
  screenWidth(screensize){
    if(Number(screensize)>1300){
      this.desktopView=true
      this.tabView=false
    }
    else{
      this.tabView=true
      this.desktopView=false
    }
  }

}
