import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  public innerWidth:any
  public desktopView:Boolean
  public tabView:Boolean
  constructor() { }
  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.screenWidth(this.innerWidth)
  }
  @HostListener('window:resize', ['$event'])
onResize(event) {
  this.innerWidth = window.innerWidth;
  this.screenWidth(this.innerWidth)
}
screenWidth(screensize){
  if(Number(screensize)>800){
    this.desktopView=true
    this.tabView=false
  }
  else{
    this.tabView=true
    this.desktopView=false
  }
}

}
