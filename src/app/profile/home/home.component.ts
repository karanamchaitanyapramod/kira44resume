import { Component, Input, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  public innerWidth:any;
  public desktopView:Boolean;
  public tabView:Boolean

  @Input() screenwidth:any

  constructor(private dialog:MatDialog) { }

  ngOnInit() {
    this.onResize()
  }
  openDialog(){
  }
  @HostListener('window:resize')
onResize() {
  this.innerWidth = window.innerWidth;
  this.screenWidth(this.innerWidth)
}
screenWidth(screensize){
  if(Number(screensize)>1224){
    this.desktopView=true
    this.tabView=false
  }
  else{
    this.tabView=true
    this.desktopView=false
  }
}

}
