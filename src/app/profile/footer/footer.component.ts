import { Component, Input, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public innerWidth:any
  public desktopView:any
  public tabView:any
  constructor() { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.screenWidth(this.innerWidth)
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.screenWidth(this.innerWidth)
  }
  screenWidth(screensize){
    if(Number(screensize)>1300){
      this.desktopView=true
      this.tabView=false
    }
    else{
      this.tabView=true
      this.desktopView=false
    }
  }

}
