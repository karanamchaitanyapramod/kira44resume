import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public desktopView=false
  public tabView=false
  public innerWidth:any
  public homeActive=true;
  public aboutActive=false
  public educationActive=false;
  public experienceActive=false;
  public skillsActive=false;
  public contactActive=false;
  constructor() { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.screenWidth(this.innerWidth)

  }
  areas=["home","about","experience","education","skills","contact"]
  highlightSection(section){
    if(section=="home"){
      this.setall()
      this.homeActive=true;
    }
    else if(section=="about"){
      this.setall()
      this.aboutActive=true
    }
    else if(section=="experience"){
      this.setall()
      this.experienceActive=true;
    }
    else if(section=="education"){
      this.setall();
      this.educationActive=true;
    }
    else if(section=="skills"){
      this.setall();
      this.skillsActive=true;
    }
    else if(section=="contact"){
      this.setall();
      this.contactActive=true
    }
  }
  setall(){
   this.homeActive=false;
   this.aboutActive=false
   this.educationActive=false;
   this.experienceActive=false;
   this.skillsActive=false;
   this.contactActive=false;
  }
  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
  const activeElem = this.areas[
      Math.floor(window.pageYOffset/ document.documentElement.clientHeight)
  ];
  this.highlightSection(activeElem)
}
@HostListener('window:resize', ['$event'])
onResize(event) {
  this.innerWidth = window.innerWidth;
  this.screenWidth(this.innerWidth)
}
screenWidth(screensize){
  if(Number(screensize)>800){
    this.desktopView=true
    this.tabView=false
  }
  else{
    this.tabView=true
    this.desktopView=false
  }
}


}
